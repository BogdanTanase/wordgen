//Developed by Bogdan Tanase
//June-2024

var i64b = 2**64;
var ns_max = 20;

var alfa='abcdefghijklmnopqrstuvwxyz';

var beta=[];
for (let i =0; i < alfa.length; i++) {
	for (let j =0; j < alfa.length; j++) {
		beta.push(alfa[i]+alfa[j]);
	}
}

var gama=[];
for (let i =0; i < beta.length; i++) {
	for (let j =0; j < beta.length; j++) {
		gama.push(beta[i]+beta[j]);
	}
}

class Distribution {
	zero = String(0);
	constructor(datastring, id) {
		if (typeof datastring === "string") {
			this.datastring = String(datastring);
		}
		else if (typeof datastring === "object" && datastring.length > 0) {
			this.dataArray = datastring;
			this.dataArrayBegin = Array(datastring.length).fill(0);
		}
		this.id = id;
		//
		this.dataseparator = ',';
		this.datalineEndchar = '\n';
		//
		this.validItems = [];
		//
		this.itemPos = [];
		if (typeof this.dataArray !== 'undefined') {
			let i = 0;
			for (const elem of this.dataArray) {
				this.dataArrayBegin[i] = this.itemPos.length;
				this.initItemPos(elem);
				i++;
			}
		}
		else {this.initItemPos(this.datastring);}		
	}
	
	pushItem(b, e, data) {
		this.itemPos.push([b, e]);
		if (this.checkItem(b, e, data)) {
			this.validItems.push(this.itemPos.length - 1);
		}
	}
	
	initItemPos(data) {
		console.log('Initializing data for : ' + this.id);
		let b = 0;
		let e = data.length - 1;
		let p;
		let p0 = data.indexOf(this.datalineEndchar, b);
		if (p0 != -1) {
			this.pushItem(b, p0-1, data);
			b = p0 + 1;
			while (b <= e && e - b > p0) {
				p = data.indexOf(this.datalineEndchar, b);
				this.pushItem(b, p-1, data);
				b = p + 1;
			}
		}
		this.pushItem(b, e, data);
		console.log('\tFinished initializing data.');
	}

	getItem(itemNr) {
		let b = this.itemPos[itemNr][0];
		let e = this.itemPos[itemNr][1];
		
		if (typeof this.dataArray !== 'undefined') {	
			if (this.validItems.indexOf(itemNr) !== -1) {
				let n = -1;
			
				for (let i = this.dataArrayBegin.length - 1; i >= 0 ; i--) {
					if (itemNr >= this.dataArrayBegin[i]) {
						n = i;
						break;
					}
					
				}
			
				let data = this.dataArray[n].slice(b, e);
				if (this.checkItem(b, e, this.dataArray[n])) {
					let dArray = data.split(this.dataseparator);
					for (let i = 0; i < dArray.length; i++) {
						dArray[i] = Number(dArray[i]);
					}
					return dArray;
				}
			}
		}
		else {
			if (this.validItems.indexOf(itemNr) !== -1) {
				let data = this.datastring.slice(b, e);
				if (this.checkItem(b, e, this.datastring)) {
					let dataArray = data.split(this.dataseparator);
					for (let i = 0; i < dataArray.length; i++) {
						dataArray[i] = Number(dataArray[i]);
					}
					return dataArray;
				}
			}
		}
		return -1;
	}
	
	checkItem(b, e, data) {
		let d = data.slice(b, e);
		return this.stringContainsDecimals(d);
	}
	
	stringContainsDecimals(s) {
		if (typeof s === "string") {
			for (let i = 1; i < 10; i++) {
				if (s.search(i) >= 0) {
					return true;
				}
			}
		}
		return false;
	}
	
	isValid() {return this.validItems.length > 0}
}

function valInRHOpenRange(val, minv, maxv) {
	return val >= minv || val < maxv;
}

function generateWord(w_length, distr) {
	for (const d of distr) {
		if (! d.isValid()) {return 'Invalid distr'}
		if (d.id === 1) {b_distr = d}
		if (d.id === 2) {b_distr2 = d}
		if (d.id === 3) {b_distr3 = d;}
		if (d.id === 4) {gama_distr = d;}
	}
	
	//w2 is the generated word, by adding 2 char elem from beta
	let w2 = '';
	//w4 is last 4 char of generated word
	let w4 = '';
	let ns = 0;
	let r = i64b;
	let ip = i64b;
	let w2o = '';
	if (w_length % 2 == 0) {
		while (w2.length < w_length) {
			r = i64b;
			if (w2 === '') {
				r = weighted_random(rangeArray(beta.length), b_distr.getItem(0));
			}
			else if (w2.length > 3) {
				ip = i64b;
				let ip1 = i64b;
				let ip2 = i64b;
				let ip3 = i64b;
				let ip4 = i64b;
				w4 = w2.slice(-4, w2.length);
				ip1= alfa.indexOf(w4[0]);
				ip2= alfa.indexOf(w4[1]);
				ip3= alfa.indexOf(w4[2]);
				ip4= alfa.indexOf(w4[3]);
				if (ip1 !== i64b && ip2 !== i64b && ip3 !== i64b && ip4 !== i64b) {
					ip = ip1*alfa.length**3 + ip2*alfa.length**2 + ip3*alfa.length + ip4;
				}

				r = getW2FromDistrib(gama_distr, w2o, ns, ip);
			}
			else {r = getW2FromDistrib(b_distr2, w2o, ns);}
			
			if (r !== i64b) {
				w2 += beta[r];
				w2o = beta[r];
			}
			else {
				break;
			}
			ns += 1;
		}
	}
	else {
		while (w2.length < w_length) {
			console.log('ns: '+ ns);
			r = i64b;
			if (w2 === '') {
				do {
					r = Math.floor(Math.random() * alfa.length);
				} while (b_distr3.validItems.indexOf(r) === -1);
				w2 += alfa[r];
				w2o = alfa[r];
			}
			else {
				ip = i64b;
				if (w2o.length === 1) {
					ip = alfa.indexOf(w2o);
					if (ip !== i64b  && ip !== -1) {
						if (b_distr3.validItems.indexOf(ip) !== -1) {
							r = weighted_random(rangeArray(beta.length), b_distr3.getItem(ip));
						}
						else {
							console.log('case not covered:1')
						}
					}
					else {
						console.log('case not covered:2')
					}
				}
				else {r = getW2FromDistrib(b_distr2, w2o, ns);}			
				
				if (r !== i64b) {
					w2 += beta[r];
					w2o = beta[r];
				}
				else {
					break;
				}
			}
			ns += 1;
		}
	}
	//console.log('w2:: '+ String(w2));
	return w2;
}

function getW2FromDistrib(distrib, w2o, ns, ip = -1) {
	let r = i64b;
	if (ip === -1) {
		ip = beta.indexOf(w2o);
		ip = ip * ns_max + ns;
	
		if (distrib.validItems.indexOf(ip) !== -1) {
			r = weighted_random(rangeArray(beta.length), distrib.getItem(ip));
		}
		else {
			console.log('case not covered:3')
			
			let n = ns
			
			while(n < ns_max) {
				n += 1;
				//console.log('w2o::ip::n:: '+w2o+' '+ip+' '+n)
				ip = beta.indexOf(w2o);
				ip = ip * ns_max + n;
				if (distrib.validItems.indexOf(ip) !== -1) {
					r = weighted_random(rangeArray(beta.length), distrib.getItem(ip));
					break;
				}
			}
		}
	}
	else {
		if (distrib.validItems.indexOf(ip) !== -1) {
			r = weighted_random(rangeArray(beta.length), distrib.getItem(ip));
		}
	}
	return r;
}

//from https://stackoverflow.com/questions/43566019/how-to-choose-a-weighted-random-array-element-in-javascript
//takes two same-length lists, items and weights, and 
//returns a random item from items according to their corresponding likelihoods of being chosen in weights. 
//The weights array should be of nonnegative numbers, but it doesn't need to sum to a particular value, and
// it can handle non-integer weights.
function weighted_random(items, weights) {
    var i;

    for (i = 1; i < weights.length; i++)
        weights[i] += weights[i - 1];
    
    var random = Math.random() * weights[weights.length - 1];
    
    for (i = 0; i < weights.length; i++)
        if (weights[i] > random)
            break;

    return items[i];
}

//from W3schools
function getRndInteger(min, max) {
	return Math.floor(Math.random() * (Number(max) - Number(min) + 1) ) + Number(min);
}

//
function rangeArray(arrayLength) {
	let rangeArray = [];
	for (let i = 0; i < arrayLength; i++) {
		rangeArray.push(Number(i));
	}
	return rangeArray;
}

//from https://stackoverflow.com/questions/76691769/how-to-use-decompressionstream-to-decompress-a-gzip-file-using-javascript-in-bro
const decompress = async (url) => {
  const ds = new DecompressionStream('gzip');
  const response = await fetch(url);
  const blob_in = await response.blob();
  const stream_in = blob_in.stream().pipeThrough(ds);
  const blob_out = await new Response(stream_in).blob();
  return await blob_out.text();
};

async function getData(dataUrls) {
	let r = '';
	//avoid CORS err by using free online proxy site
	let url_proxy = 'https://corsproxy.io/?';
	url_proxy = '';///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	for (const url_data of dataUrls) {
		const url = url_proxy + encodeURIComponent(url_data);
		const result = await decompress(url);
		r += result;
	}
	return r;
}

function setDistr(urlArray, distr, id){
	let res = '';
	res = getData(urlArray);

	var data = '';
	res.then((result) => {data = result});
	
	return Promise.allSettled([res]).then((result) => {distr.push(new Distribution(data, id));});
}

function getDistr(urlArray, distr){
	let res = '';
	res = getData(urlArray);

	var data = '';
	res.then((result) => {data = result});
	
	return Promise.allSettled([res]).then((result) => {distr2.push(data);});
}


//UI
function addButton() {
	const btn = document.createElement("input");
	btn.type = 'button';
	btn.value = 'Generate';
	btn.setAttribute('id', 'genbut');
	btn.setAttribute('onclick', 'genWords()');
	btn.setAttribute('disabled', '');
	document.body.appendChild(btn);
	
	const para = document.createElement("h2");
	para.setAttribute('id', 'genbutinfo');	
	para.innerText = "Wait a while to load data ...\n";
	para.style.color = 'red'
	document.body.appendChild(para);
}

addButton();

const para = document.createElement("p");
para.innerText = "Number of words to generate";
document.body.appendChild(para);

const inp_NW = document.createElement("input");
inp_NW.value = 10;
document.body.appendChild(inp_NW);

const para2 = document.createElement("p");
para2.innerText = "min and max word lengh";
document.body.appendChild(para2);

const inp_min = document.createElement("input");
inp_min.value = 3;
document.body.appendChild(inp_min);

const inp_max = document.createElement("input");
inp_max.value = 14;
document.body.appendChild(inp_max);


//generate
function genWords() {
	let word;
	let w_length;
	let word_out = '';
	
	for (let i = 0; i < inp_NW.value; i++) {
		w_length = getRndInteger(inp_min.value, inp_max.value);
		console.log('w_length:'+ w_length);
		word = generateWord(w_length, distr);
		word_out += 'w_length:'+ w_length + ' :: ' + word + '\n';
	}

	const para3 = document.createElement("p");
	para3.innerText = "Generated words :\n" + word_out;
	document.body.appendChild(para3);
}

//load data
var proms = [];
var distr = [];
var distr2 = [];
//const prom1 = setDistr(['https://gitlab.com/BogdanTanase/wordgen/-/raw/main/t222_1.gz'], distr);
//const prom2 = setDistr(['https://gitlab.com/BogdanTanase/wordgen/-/raw/main/t222_2.gz'], distr);
//const prom3 = setDistr(['https://gitlab.com/BogdanTanase/wordgen/-/raw/main/t222_3.gz'], distr);

proms.push(setDistr(['t222_1.gz'], distr, 1));
proms.push(setDistr(['t222_2.gz'], distr, 2));
proms.push(setDistr(['t222_3.gz'], distr, 3));
//

console.log('len:proms '+ proms.length);

Promise.allSettled(proms.slice(0, 3)).then((result) => {
	proms.push(getDistr(['t222_4_1.gz'], distr2));
	Promise.allSettled([proms[3]]).then((result) => {
		proms.push(getDistr(['t222_4_2.gz'], distr2));
		Promise.allSettled([proms[4]]).then((result) => {
			proms.push(getDistr(['t222_4_3.gz'], distr2));
			Promise.allSettled([proms[5]]).then((result) => {
				proms.push(getDistr(['t222_4_4.gz'], distr2));
				Promise.allSettled([proms[6]]).then((result) => {
					proms.push(getDistr(['t222_4_5.gz'], distr2));
					Promise.allSettled([proms[7]]).then((result) => {
						proms.push(getDistr(['t222_4_6.gz'], distr2));
						Promise.allSettled([proms[8]]).then((result) => {
							proms.push(getDistr(['t222_4_7.gz'], distr2));
							Promise.allSettled(proms.slice(3, 10)).then((result) => {
								console.log('len:'+ distr.length);
								
								const d3 = new Distribution(distr2, 4);
								distr.push(d3);
								
								const para = document.getElementById("genbutinfo");
								para.innerText = "Ready to generate words !\n";
								para.style.color = 'green'
								
								const btn = document.getElementById("genbut");
								btn.removeAttribute('disabled');
							});
						});
					});
				});
			});
		});
	});
});
	
	
